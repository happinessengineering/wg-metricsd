clean: ## remove the build assets
clean:
	rm -rf build/

build-frontend: ## build the frontend
build-frontend:
	cd app/frontend; \
	  go build -o ../../build/wg-tray

build-backend: ## build the backend
build-backend:
	cd app/backend; \
	  go build -o ../../build/wg-metricsd
module gitlab.com/happinessengineering/wg-metricsd

go 1.13

require (
	github.com/GeertJohan/go.rice v1.0.0 // indirect
	github.com/getlantern/systray v1.0.4
	github.com/mitchellh/go-homedir v1.1.0
)

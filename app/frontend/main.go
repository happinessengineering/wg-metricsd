package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/getlantern/systray"

	"gitlab.com/happinessengineering/wg-metricsd/icon"
)

const (
	serverAddr  = "http://localhost"
	serverPort  = ":9555"
	metricsPath = "/metrics"
)

var (
	isConnected = false
)

func main() {
	onExit := func() {
		fmt.Println("Goodbye")
	}

	systray.Run(onReady, onExit)
}

func onReady() {

	systray.SetTemplateIcon(icon.TrayIconDisabled, icon.TrayIconDisabled)

	systray.SetTooltip("Wireguard Status")

	mQuitOrig := systray.AddMenuItem("Quit", "Exit the application")
	go func() {
		<-mQuitOrig.ClickedCh
		systray.Quit()
	}()

	go func() {

		for {

			res, err := http.Get(serverAddr + serverPort + metricsPath)

			if err != nil {
				panic(err)
			}

			data, _ := ioutil.ReadAll(res.Body)

			var wgDevices []WgDevice

			json.Unmarshal(data, &wgDevices)

			if len(wgDevices) > 0 {
				if !isConnected {
					systray.SetTemplateIcon(icon.TrayIconEnabled, icon.TrayIconEnabled)
					osNotify("Wireguard", "Connected!", "wgNotificationIconEnabled")
					isConnected = true
				}
			} else {
				if isConnected {
					systray.SetTemplateIcon(icon.TrayIconDisabled, icon.TrayIconDisabled)
					osNotify("Wireguard", "Disconnected!", "wgNotificationIconDisabled")
					isConnected = false
				}
			}

			time.Sleep(time.Second * 1)

		}

	}()

}

package main

import (
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"syscall"

	"github.com/mitchellh/go-homedir"

	"gitlab.com/happinessengineering/wg-metricsd/icon"
)

// osNotify - notify the os
func osNotify(title, message, notificationIconName string) error {

	err := writeIcon()
	if err != nil {
		return err
	}

	send, err := exec.LookPath("sw-notify-send")
	if err != nil {
		send, err = exec.LookPath("notify-send")
		if err != nil {
			return err
		}
	}

	c := exec.Command(send, title, message, "-i", notificationIconName)

	if (len(os.Getenv("SUDO_UID")) > 0) && (os.Geteuid() == 0) {

		callerUID, err := strconv.Atoi(os.Getenv("SUDO_UID"))
		if err != nil {
			return err
		}
		callerGID, err := strconv.Atoi(os.Getenv("SUDO_GID"))
		if err != nil {
			return err
		}
		c.SysProcAttr = &syscall.SysProcAttr{
			Credential: &syscall.Credential{
				Uid: uint32(callerUID),
				Gid: uint32(callerGID),
			},
		}
	}

	return c.Start()

}

func writeIcon() error {

	assetDir := "/.local/share/icons"

	ie := icon.NotifyIconEnabled
	ieNotificationImagePath := "/wgNotificationIconEnabled.png"

	id := icon.NotifyIconDisabled
	idNotificationImagePath := "/wgNotificationIconDisabled.png"

	homeDir, err := homedir.Dir()
	if err != nil {
		return err
	}

	if _, err := os.Stat(homeDir + assetDir + ieNotificationImagePath); os.IsNotExist(err) {

		if _, err := os.Stat(homeDir + assetDir); os.IsNotExist(err) {
			os.Mkdir(homeDir+assetDir, os.ModePerm)
		}
		err := ioutil.WriteFile(homeDir+assetDir+ieNotificationImagePath, ie, 0444)
		if err != nil {
			return err
		}
		err = ioutil.WriteFile(homeDir+assetDir+idNotificationImagePath, id, 0444)
		if err != nil {
			return err
		}
	}

	return nil
}

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
)

func (wgc *WgClient) metricsHandler(w http.ResponseWriter, r *http.Request) {

	devices, err := wgc.Client.Devices()
	if err != nil {
		w.Write(sendError(500, err.Error()))
		return
	}

	if devices == nil {
		devices = []*wgtypes.Device{}
	}

	var returnDevices []*WgDevice

	for _, d := range devices {

		var td WgDevice

		td.Type = d.Type.String()
		td.PublicKey = d.PublicKey.String()
		td.PrivateKey = d.PrivateKey.String()
		td.Name = d.Name
		td.ListenPort = d.ListenPort
		td.Firewallmark = d.FirewallMark

		for _, p := range d.Peers {

			var tp Peer

			tp.Endpoint = p.Endpoint
			tp.LastHandshakeTime = p.LastHandshakeTime
			tp.PersistentKeepaliveInterval = p.PersistentKeepaliveInterval
			tp.PresharedKey = p.PresharedKey.String()
			tp.ProtocolVersion = p.ProtocolVersion
			tp.PublicKey = p.PublicKey.String()
			tp.ReceiveBytes = p.ReceiveBytes
			tp.TransmitBytes = p.TransmitBytes

			for _, ip := range p.AllowedIPs {

				var tip AllowedIP

				maskSize, _ := ip.Mask.Size()

				tip.IP = ip.IP
				tip.MaskSlash = "/" + strconv.Itoa(maskSize)

				tip.MaskIP, err = ipv4MaskString(ip.Mask)
				if err != nil {
					tip.MaskIP = err.Error()
				}

				tp.AllowedIPs = append(tp.AllowedIPs, &tip)
			}

			td.Peers = append(td.Peers, &tp)
		}

		returnDevices = append(returnDevices, &td)

	}

	if returnDevices == nil {
		returnDevices = []*WgDevice{}
	}

	returnJSON, _ := json.Marshal(returnDevices)

	w.Write(returnJSON)

	return

}

func sendError(code int, message string) []byte {

	e := Error{
		Code:    code,
		Message: message,
	}

	r, _ := json.Marshal(e)
	return r

}

func ipv4MaskString(m []byte) (string, error) {
	if len(m) != 4 {
		return "", errors.New("ipv4Mask: len must be 4 bytes")
	}

	return fmt.Sprintf("%d.%d.%d.%d", m[0], m[1], m[2], m[3]), nil
}

func main() {

	newClient, err := wgctrl.New()
	if err != nil {
		panic(err)
	}
	w := &WgClient{Client: newClient}

	http.HandleFunc("/metrics", w.metricsHandler)
	http.ListenAndServe(":9555", nil)

}

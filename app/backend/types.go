package main

import (
	"net"
	"time"

	"golang.zx2c4.com/wireguard/wgctrl"
)

// Error - an error
type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// WgClient - a Wireguard client
type WgClient struct {
	Client *wgctrl.Client
}

// WgDevice - a Wireguard connection object
type WgDevice struct {
	Name         string  `json:"name"`
	Type         string  `json:"type"`
	PrivateKey   string  `json:"private_key"`
	PublicKey    string  `json:"public_key"`
	ListenPort   int     `json:"listen_port"`
	Firewallmark int     `json:"firewall_mark"`
	Peers        []*Peer `json:"peers"`
}

// Peer - a Wireguard peer object
type Peer struct {
	PublicKey                   string        `json:"public_key"`
	PresharedKey                string        `json:"preshared_key"`
	Endpoint                    *net.UDPAddr  `json:"endpoint"`
	PersistentKeepaliveInterval time.Duration `json:"persistent_keepalive_interval"`
	LastHandshakeTime           time.Time     `json:"last_handshake_time"`
	ReceiveBytes                int64         `json:"receive_bytes"`
	TransmitBytes               int64         `json:"transmit_bytes"`
	AllowedIPs                  []*AllowedIP  `json:"allowed_ips"`
	ProtocolVersion             int           `json:"protocol_version"`
}

// AllowedIP - a Wireguard AllowedIP object
type AllowedIP struct {
	IP        net.IP `json:"ip"`
	MaskSlash string `json:"mask_slash"`
	MaskIP    string `json:"mask_ip"`
}

## wg-metricsd

Wireguard is great, but the client ecosystem is... not so great.

If you use the `wg-quick` command line tool to manage connectivity, there's no easy way to identify connection state: is there an active private network or not?

This project is a dirt simple fix: a small application which exposes a REST API a client can query for connection state.

```
$ curl -s localhost:9555/metrics | jq
[
  {
    "name": "MyWgConnection",
    "type": "Linux kernel",
    "private_key": "someprivatekey",
    "public_key": "somepublickey",
    "listen_port": 39404,
    "firewall_mark": 0,
    "peers": [
      {
        "public_key": "somepeerpublickey",
        "preshared_key": "somepeerpresharedkey",
        "endpoint": {
          "IP": "55.55.55.55",
          "Port": 1194,
          "Zone": ""
        },
        "persistent_keepalive_interval": 25000000000,
        "last_handshake_time": "2021-01-01T01:00:00.0000000-04:00",
        "receive_bytes": 13700,
        "transmit_bytes": 12268,
        "allowed_ips": [
          {
            "ip": "10.0.0.0",
            "mask_slash": "/16",
            "mask_ip": "255.255.0.0"
          }
        ],
        "protocol_version": 1
      }
    ]
  }
]
```

Included is a sample client, a small system tray which notifies of connection/disconnection.

Quick start:

To build the `wg-metricsd` binary:
`$ make build-backend`

Run the binary as root:
`$ build/wg-metricsd &`

(Optional) To build the sample system tray client:
`$ make build-frontend`

Run the tray application:
`$ build/wg-tray &`